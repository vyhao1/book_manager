#!/bin/bash

service_name=$1
port=$2

while ! nc -z $service_name $port; do
  sleep 1
done