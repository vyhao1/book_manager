package com.example.book.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.book.entity.Book;

@Service
public interface BookService {
	public List<Book> getAllBooks();
	public Book getBookById(Integer id);
	public Book addBook(Book book);
	public String deleteBookById(Integer id);
	public Book updateBook(Integer id, Book book);
}
