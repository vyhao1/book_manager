package com.example.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.book.entity.Book;
import com.example.book.repository.BookRepository;

import jakarta.transaction.Transactional;

import java.util.List;

@Service
@Transactional
public class BookServiceImpl   implements BookService{

    @Autowired
	private BookRepository repository;

    @Override
	public List<Book> getAllBooks() {
		return repository.findAll();
	}

	@Override
	public Book getBookById(Integer id) {
		return repository.findById(id).get();
	}

	@Override
	public Book addBook(Book book) {
		return repository.save(book);
	}

	@Override
	public String deleteBookById(Integer id) {
		repository.deleteById(id);
		return "Bạn đã xóa Book thành công!";
	}

	@Override
	public Book updateBook(Integer id, Book bookNew) {
		Book book = repository.findById(id).orElse(null);
		if (book != null) {
			if (bookNew.getName() != null && !bookNew.getName().isEmpty()) {
				book.setName(bookNew.getName());
				book.setAuthor(bookNew.getAuthor());
				book.setPrice(bookNew.getPrice());
				return repository.save(book);
			}
		}
		return null;
	}

   

}
