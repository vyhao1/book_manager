package com.example.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.entity.Book;
import com.example.book.service.BookService;
import com.example.book.service.BookServiceImpl;

import java.util.List;

@RestController()
@RequestMapping("/api/v1")
public class BookController {
	@Autowired
	private BookService bookService;

    @GetMapping("books")
    public List<Book> getAllBooks(){
        return bookService.getAllBooks(); 
    }
    
 	@PostMapping("/books")
 	public Book addBook(@RequestBody Book book) {
 		return bookService.addBook(book);
 	}

    @GetMapping("/books/{id}")
    public Book getBookById(@PathVariable Integer id){
        return bookService.getBookById(id);
    }
    
    @DeleteMapping("/books/{id}")
    public String deleteBookById(@PathVariable Integer id) {
    	bookService.deleteBookById(id);
    	return "Bạn đã xóa Book thành công!";
    }
    
 	@PatchMapping("/books/{id}")
 	public Book updateBook(@PathVariable Integer id, @RequestBody  Book book) {
 		return bookService.updateBook(id, book);
 	}
}
